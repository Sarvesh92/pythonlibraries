import json
from dataclasses import dataclass, field
from time import perf_counter


@dataclass(frozen=True, match_args=True, slots=True)
class Person:
    first_name: str
    last_name: str
    age: int
    address: str
    salary: int
    email: str
    phone_numbers: list[int] = field(default_factory=list)


if __name__ == "__main__":
    with open(f"persons.json", "r") as f:
        persons = [Person(**p) for p in json.load(f)]
        print(persons)
