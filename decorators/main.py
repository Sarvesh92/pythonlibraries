import time
from typing import Callable


def timer(func: Callable) -> Callable:
    """Print the runtime of the decorated function"""

    def wrapper(*args, **kwargs):
        print(f"Start execution {func.__name__}")
        start = time.time()
        rv = func(*args, **kwargs)
        end = time.time()
        print(f"End execution {func.__name__} and time taken in seconds {end-start}")
        print(f"{rv=}")
        return rv

    return wrapper


@timer
def add(x, y):
    return x + y


@timer
def multiply(a, b):
    return a * b


multiply(1, 2)
add(1, 2)
