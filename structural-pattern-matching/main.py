from typing import Any


def spm_on_abs(*, command: str) -> None:
    """
    Run a command and return the output.

    param command: Represents command
    type command: str
    """

    match command:

        case "quit":
            print("Quitting...")

        case "help":
            print("Available commands:")

        case _:
            print(f"Unknown command: {command!r}")


def spm_on_ls(*, command: str):

    """
    Run a command and return the output.

    param command: Represents command
    type command: str

    rtype: None
    """
    dirname = "test"

    match command.split():

        case ["quit" | "exit"]:
            print("Quitting...")

        case ["rm", direname, "-rf"] if direname != dirname:
            print(f"Removing all files from directory:{direname!r}")

        case _:
            print("Unknown command")


def spm_on_dict(*, command: dict[str, Any]):
    """
    Pattern match dictionary

    param command: Represents command
    type command: dict[str, Any]

    rtype: None
    """

    match command:
        case {"command": "quit", "args": args}:
            print(f"Quitting {args}")

        case {"command": "rm", "dirname": dirname, "args": args}:
            print(f"Removing files: {args} from {dirname= !r}")

        case _:
            print("Unknown command")


if __name__ == "__main__":
    spm_on_abs(command="quit")
    spm_on_ls(command="rm users -rf")
    spm_on_dict(
        command={"command": "rm", "dirname": "test", "args": ["f1.txt", "f2.txt"]}
    )
