from functools import cache
import time


def calculate_time(func):
    print("Calculating time...")

    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f"Time taken by {func.__name__} is {end - start}")
        return result

    return wrapper


@cache
@calculate_time
def sum_digits(n) -> int:
    """
    Define a function sum_digits(n) that takes a positive integer n
    and returns the sum of all its digits.

    param n: positive integer
    type n: int

    return: sum of all digits
    rtype: int
    """

    assert n >= 0, f"input {n} must be a positive integer"
    if n == 0:
        return n
    return (n % 10) + sum_digits(n // 10)


@cache
@calculate_time
def calculate_power(number, p) -> int:
    if p == 0:
        return 1
    return number * calculate_power(number, p - 1)


@cache
@calculate_time
def calculate_gcd(a, b) -> int:
    """
    Define a function gcd(a, b) that takes two positive integers a and b
    and returns their greatest common divisor.

    We are using Euclidean algorithm to calculate GCD.
    e.g 48,18
    lowest 18 -> 48 % 18 -> 2 and reminder 12
    lowest 12 -> 18 % 12 -> 1 and reminder 6
    lowest 6 -> 12 % 6 -> 2 and reminder 0
    6 is the lowest number and reminder is 0, so 6 is the GCD

    param a: positive integer
    type a: int

    param b: positive integer
    type b: int

    return: greatest common divisor
    rtype: int
    """

    assert a >= 0 and b >= 0, f"input {a} and {b} must be positive integers"
    a, b = reversed(sorted([a, b]))
    if b == 0:
        return a
    return calculate_gcd(b, a % b)


def convert_decimal_binary(num):
    if num == 1:
        return 1
    return f"{convert_decimal_binary(num // 2)}{str(num % 2)}"


def run():
    while (ip := input("Enter a positive integer and power: ")) != "":
        n, p = ip.split()
        print(f"Sum of digits: {sum_digits(int(n))}")
        print(f"Power: {calculate_power(int(n), int(p))}")
        print(f"GCD: {calculate_gcd(int(n), int(p))}")
        print((convert_decimal_binary(int(n))))


if __name__ == "__main__":
    run()
