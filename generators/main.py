class Gen:
    def __init__(self, end: int) -> None:
        self.end: int = end
        self.current: int = 0

    def __next__(self) -> int:
        return self.next()

    def next(self) -> int:
        if self.current == self.end:
            raise StopIteration
        self.current += 1
        return self.current


if __name__ == "__main__":
    gen = Gen(1000)
    while True:
        try:
            print(next(gen))
        except StopIteration:
            break
