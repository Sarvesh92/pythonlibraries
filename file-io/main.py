from asyncore import read
import csv
import json
from textwrap import indent


def read_file(filename):
    with open(filename, "r", newline="") as f:
        reader = csv.DictReader(f)
        for row in reader:
            yield row


def process_data(op_fmt: str):

    match op_fmt:

        case "csv":
            data = read_file("mock_data.csv")
            headers = next(data)
            with open("output.csv", "w") as f:
                writer = csv.DictWriter(f, fieldnames=headers)
                writer.writeheader()
                for row in data:
                    writer.writerow(row)

        case "json":
            with open("output.json", "w") as f:
                json.dump(list(read_file("mock_data.csv")), f, indent=2)

        case "db":
            pass

        case _:
            raise ValueError("Invalid format")


if __name__ == "__main__":
    process_data("json")
