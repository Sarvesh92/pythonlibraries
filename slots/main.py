import decimal
from functools import partial
import timeit


class Person_No_Slots:
    def __init__(self, *, name: str, age: int, salary: decimal.Decimal):
        self.name = name
        self.age = age
        self.salary = salary

    def __repr__(self):
        return f"Persona(name={self.name},age={self.age},salary={self.salary})"


class Person_Slots:
    __slots__ = ["name", "age", "salary", "address"]

    def __init__(self, *, name: str, age: int, salary: decimal.Decimal):
        self.name = name
        self.age = age
        self.salary = salary

    def __repr__(self) -> str:
        return f"Persona(name={self.name},age={self.age},salary={self.salary})"


def get_set_del(*, person: Person_No_Slots | Person_Slots):
    person.name
    person.address = "JuanJuanJuanJuanJuanJuanJuanJuanJuanJuanJuanJuanJuanJuan"
    del person.address


if __name__ == "__main__":
    person = Person_No_Slots(name="John", age=30, salary=decimal.Decimal("1000.00"))

    # compare performance with and without slots using timeit
    with_no_slots = min(
        timeit.repeat(
            partial(
                get_set_del,
                person=Person_No_Slots(
                    name="John", age=30, salary=decimal.Decimal("1000.00")
                ),
            ),
            number=10000,
        )
    )

    with_slots = min(
        timeit.repeat(
            partial(
                get_set_del,
                person=Person_Slots(
                    name="John", age=30, salary=decimal.Decimal("1000.00")
                ),
            ),
            number=10000,
        )
    )
    performance_improvement: float = (with_no_slots - with_slots) / with_no_slots
    print(
        f"Performance improvement % is {with_no_slots=}:{with_slots=} {performance_improvement:.2%}"
    )
